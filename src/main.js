import Vue from 'vue'
import VueRouter from 'vue-router'
import Vue2Filter from 'vue2-filters'


//components
import Auth from './components/Auth.vue'
import PhoneBook from './components/Phones.vue'
import {
  url,checkIsAuth
} from './Api.js'

Vue
  .use(VueRouter)
  .use(Vue2Filter);

const router = new VueRouter({
  mode: 'history',
  routes: [{
    path: '/auth',
    name: 'auth',
    component: Auth
  }, {
    path: '/phones',
    name: 'phones',
    component: PhoneBook
  }]
})

export function navigate(name) {
  router.push({
    name: name
  })
}

export function navigateWithParams(name, params) {
  router.push({
    name: name,
    params: params
  })
}

var app = new Vue({
  el: '#app',
  router: router
})

checkIsAuth()
