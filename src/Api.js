import {
  navigate
} from './main.js'
import axios from 'axios'

export const url = 'https://mail-contacts-api.herokuapp.com/api'
// export const url = 'http://localhost:3000/api'

export function auth() {
  axios({
    method: 'post',
    url: `${url}/auth`,
    data: {
      login: document.querySelector("input[name='login']").value,
      password: document.querySelector("input[name='password']").value
    },
    withCredentials: true
  }).then(() => {
    navigate("phones")
  }).catch(error => {
    console.log(error)
  })
}

export function logout() {
  axios({
    url: `${url}/logout`,
    method: "post",
    withCredentials: true,
  }).then(() => {
    navigate("auth")
  }).catch(error => {
    console.log(error)
  })
}

export function checkIsAuth() {
  axios({
    url: `${url}/user`,
    method: "get",
    withCredentials: true
  }).then((obj) => {
    navigate("phones")
  }).catch(error => {
    navigate("auth")
  })
}

export async function updateContact(body) {
  const res = await axios({
    url: `${url}/phones/update`,
    method: "post",
    withCredentials: true,
    data: body,
    responseType: "json"
  })
  return res
}

export async function getContacts() {
  const res = await axios({
    url: `${url}/phones`,
    method: "get",
    withCredentials: true
  })
  return res
}

export async function getUser() {
  const res = await axios({
    url: `${url}/user`,
    method: "get",
    withCredentials: true
  })
  return res
}

export async function createContact(body) {
  const res = await axios({
    url: `${url}/phones/create`,
    method: "post",
    withCredentials: true,
    data: body
  })
  return res
}

export async function deleteContact(id) {
  const res = await axios({
    url: `${url}/phones/${id}`,
    method: "delete",
    withCredentials: true
  })
  return res
}

export async function deleteMultipleContacts(ids){
  const res = await axios({
    url:`${url}/phones/delete`,
    method:"post",
    withCredentials:true,
    data:ids
  })
  return res
}
